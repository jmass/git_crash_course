# git (and gitlab) basics - a crash course

# Overview

* What is git and why is it useful?
  * Some terminology
* What is gitlab?
* Walkthrough: Setting up and working on a project on gitlab.com
 	* Git installation, gitlab.com basics
 		* Setting up ssh keys
        * Create a project and cloning the repository
 	* Adding files, committing and pushing changes
 	* Basic branching and merging
 	* Resolving a merge conflict
* Dos and don'ts
  * Where (not) to store data?
  * .gitignore file
  * git LFS

# What is git and why is it useful?
* Powerful distributed version control system 
	* DB of snapshots of files in a directory ("repository")
  * Distributed: 
    * All "clones" are equal, no centralized server necessary
* Graph structure
  * branching supported, e.g. for parallel development
* git is free open source software
	* Many IDEs with git support; GUIs etc available
* Invaluable for collaborating on a project
* De facto standard for open source (and commercial) software development
* Can be a bit overwhelming at times
    * There's often more than one way to achieve something

# Some terminology 
* graph-related things:
  * ``commit`` ~ snapshot of tracked files in directory with meta data (author, timestamp, commit message, parent(s))
     * ~ node in graph
     * can have one or more parent commits (merge commit)
     * has a SHA-1 name / unique identifier
  * ``branch`` ~ line of development; pointer to a commit
  * ``HEAD`` ~ "You are here."
    * pointer to current commit in the current branch of the graph
    * ``HEAD~n`` = *n*th ancestor of ``HEAD``
    * ``HEAD~n^m`` = *m*th parent of *n*th ancestor of ``HEAD``, e.g ``HEAD~1^2`` is the second parent of the merge commit before ``HEAD``
* usage:
  * ``working tree``  ~ working directory
  * ``add`` ~ track a file or mark changes for next ``commit``
  * ``staging area`` ~ aka ``index``, state between ``add`` and ``commit``
  * ``commit`` (verb) ~ create a snapshot incl. all staged changes 
  * ``push`` ~ send changes to ``remote`` and publish
  * ``remote``  ~ repository that you ``push`` to and ``pull`` from (e.g. on gitlab.com)
  * ``pull`` = ``fetch`` and ``merge``
  * ``fetch`` ~ get changes from remote
  * ``merge`` ~ apply changes (e.g. from a ``branch``)

```mermaid
graph LR
    A(untracked file or changes) -->|git add| B(file tracked / changes staged )
    B -->|git commit| C(snapshot saved, HEAD updated)
    C-->|git push| D(remote updated / changes published) 
style B stroke:#333,stroke-width:2px,stroke-dasharray: 5 5
style A stroke:#000,stroke-width:2px,stroke-dasharray: 5 5
style C stroke:#333,stroke-width:3px
style D stroke:#333,stroke-width:4px

```

# Git commits and branches
<img src="img/git_merge_complete.png" width="30%" height="30%">

# What is gitlab?
* Hosting platform for your git repositories
    * Remote repositories can be public or private
* Offers a lot more: 
	* CI pipelines
	* Issue tracker
	* Wiki 
	* User management
	* ...
* You can host gitlab yourself (but we will only talk about [gitlab.com](https://gitlab.com) here)

# Walkthrough 

# First steps 

## git installation:
* Windows: Download from [https://git-scm.com/](https://git-scm.com/)
* Debian-based Linux: ``apt install git``

## gitlab.com account
Go to [https://gitlab.com](https://gitlab.com/) and sign up.


### Adding ssh keys to gitlab account
* Generate a key pair: [Instructions (from github.com)](https://docs.github.com/en/github/authenticating-to-github/generating-a-new-ssh-key-and-adding-it-to-the-ssh-agent)

* Copy public key and add to gitlab:
  * Open you public key with a text editor/viewer and copy it
    * e.g. ``less ~/.ssh/YOUR_KEY.pub ``
  * Go to [https://gitlab.com/-/profile/keys](https://gitlab.com/-/profile/keys) 
    and paste public key

## Creating a project on gitlab.com
* In the browser: [https://gitlab.com/projects/new](https://gitlab.com//projects/new) -> create
blank project


## Cloning the repository and configuring git

### Basic configuration (one-time setup)

``` 
   git config --global user.name "First Last"
   git config --global user.email "me@example.com" 
```

### Clone the repository (choose ssh option for convenience)
#### Example:

```
   cd ~/gitlab.com/jmass # change to dir of your choice 
   git clone git@gitlab.com:jmass/workshop_project.git
   cd workshop_project
   ls 
   ls -al
   git status
```

(Note: You can also start locally with ``git init`` instead of cloning anything. But you would have to set up a remote repository in order to ``push`` and ``pull`` changes.)

---

---

# Adding files and commiting changes
### Example:
``` 
  git status # check status 
  git help status # when in doubt: git help SUBCOMMAND
  git pull # fetch and merge changes from remote repo
  vim hello.txt # create a file and add some content
  git add hello.txt # stage contents for next commit
  git status 
  git commit -m "add greeting" # make snapshot and 
                               # add summary of 
                               # what you changed
 
  git status
  git push # update remote repo
  git log # show history of commits
```

# Working on branches
<img src="img/git_branch.png" width="30%" height="30%">


* It is a good idea to keep the main branch clean.
* You can (and should) create branches in which you can try out and implement changes before you add them to your main branch.
* There are different workflows, choose what suits you best. 
* Often, you'd want to  keep a 'dev' version of your project, and 'feature' branches in which you add code related to a specific topic that you will merge once you're happy with them. 
* Branches can be deleted again. They also don't have to be tracked on the remote repo.  

# Working on branches - before and after merge
<img src="img/git_branch.png" width="30%" height="30%"> <img src="img/git_merge_complete.png" width="30%" height="30%">

# Working on branches - before and after rebase
<img src="img/git_before_rebase.png" width="30%" height="30%"> <img src="img/git_rebase.png" width="30%" height="30%">

# Live example: branching and merging

```
   git status 
   git branch dev # create a new branch
   git status  # we're still on the main branch
   git switch dev # switch to "dev"
   ls # otherwise everthing looks the same
   vim hello.txt # modify hello.txt
   git diff # show unstaged changes
   git add hello.txt # stage changes
   git diff --staged # show staged changes
   git commit -m "add german language support"
   git push   # try to update remote; 
   # this will fail but show a helpful message
   
   git push --set-upstream origin dev # update remote
```
# Branching and merging (continued)

```  
   git status
   git branch feature_french # create another branch
   git branch feature_japanese # (for later)
   git switch feature_french
   git status
   vim hello.txt
   git add hello.txt
   git commit -m "add french language support"
   git status
   git log
   git log --oneline # easier to read

```

# Branching and merging (continued)

```
   git switch dev
   ls
   less hello.txt 
   # the changes from the other branch are not there
   git status 
   vim important.txt # add another file directly to dev
   git add important.txt
   git commit -m "very important update"
   git push # update remote
```

#  Branching and merging (continued)

```
   git switch feature_french # back to our feature branch
   git status
   vim hello.txt
   git add hello.txt 
   git commit -m "change wording"
   git log --all --decorate --oneline --graph 
   git switch dev 
   git pull # was there anything new on the remote?
   
   # incorporate changes from 
   # "feature_french" into dev
   git merge feature_french 
   # we can delete the feature branch since we're done
   git branch -d feature_french
   git push # update remote
```
# Before and after merge
## before
```
* d5bacd1 (feature_french) change wording
* 37c3715 add french language support
| * 6d100eb (HEAD -> dev) very important update
|/
* 4f1fae5 (feature_japanese) add german language support
* 08c1fc5 (origin/master, origin/HEAD, master) 
                                        add greeting
* 09fb809 Initial commit
```

## after

```
*   9f7fe47 (HEAD -> dev) Merge branch 'feature_french'
|\                                             into dev
| * d5bacd1 (feature_french) change wording
| * 37c3715 add french language support
* | 6d100eb very important update
|/
* 4f1fae5 (feature_japanese) add german language support
* 08c1fc5 (origin/master, origin/HEAD, master) 
                                        add greeting
* 09fb809 Initial commit

```

# Branching and merging (continued)

```
   git switch feature_japanese
   # imagine e.g. a coworker working on 
   # this feature while you were busy 
   # with the other feature 
   vim hello.txt
   git add hello.txt
   git commit -m "add jp"
   vim hello.txt
   git add hello.txt
   git commit -m "kana support" # create some commits
   git log --all --decorate --oneline --graph
   git rebase -i dev # let's try out rebase 
                  # instead of merge this time
                  # and clean up a bit:
                  # let's select 'drop' for ac825ad
                  # and 'reword' for 80d9f56
```

# Merge conflict
>>>
 Auto-merging hello.txt
 CONFLICT (content): Merge conflict in hello.txt

 error: could not apply 80d9f56... kana support

 Resolve all conflicts manually, mark them as resolved with

 "git add/rm <conflicted_files>", then run "git rebase --continue".

 You can instead skip this commit: run "git rebase --skip".
 
 To abort and get back to the state before "git rebase", run "git rebase --abort".
>>>

# Resolving a merge conflict

```
  # the rebase didn't go so smoothly...
  git mergetool --tool=gvimdiff 
    # or more approchable: --tool=meld
  git add hello.txt
  git rebase --continue
  git log --all --decorate --oneline --graph # ok, cool
  git switch dev 
  git pull # again, did anything change on the remote?
  git merge feature_japanese # fast forward
  git log --all --decorate --oneline --graph
  git push
  # we can also 'merge' back into main without problems
  git switch main
  git merge dev
  git push 

```
# Before and after rebase
## before
```
* 80d9f56 (HEAD -> feature_japanese) kana support
* ac825ad jp language support
| *   9f7fe47 (dev) Merge branch 'feature_french'
| |\                                        into dev
| | * d5bacd1 change wording
| | * 37c3715 add french language support
| |/
|/|   
| * 6d100eb very important update
|/  
* 4f1fae5 add german language support
* 08c1fc5 (origin/master, origin/HEAD, master) 
                                        add greeting
* 09fb809 Initial commit

```
## after
```
* ef36b25 (HEAD -> feature_japanese) add jp lang support
*   9f7fe47 (dev) Merge branch 'feature_french' into dev
|\  
| * d5bacd1 change wording
| * 37c3715 add french language support
* | 6d100eb very important update
|/  
* 4f1fae5 add german language support
* 08c1fc5 (origin/master, origin/HEAD, master) 
                                        add greeting
* 09fb809 Initial commit
``` 

# Dos and Don'ts 

# What belongs in a git repository and what does not?

A git repository is not a good place to share large files, especially if they keep being modified. 
E.g. the ``git clone`` command, copies the repository as a whole with its history, that would include all old versions of the large files. 


# What belongs in a git repository and what does not?

  | YES           | 
  | ------------- |
  | source code   | 
  | small (!) example data sets   | 
  | plain text, anything "diffable" | 
  | a readme file with instructions |
  | license information  |
  | .gitignore |

  | NO            | 
  |:-------------:|
  | compiled binaries | 
  | temporary data sets |
  | auto-generated files |
  | cached files  |    
  | dependencies|
  | virtual envs  |   
  | anything unrelated to the project  |    
  | secrets and credentials  | 
  |...|   

  | MAYBE  |
  | ------:|
  | assets/ |
  |  ...  |

# .gitignore

You can tell git to not add certain files and directories in the ``.gitignore`` file. 

Put a ``.gitignore`` in your repository and tell git which files not to track, e.g.
```
  *.log
  *.pyc
  tmp/
  # pdflatex stuff 
  *.aux
  *.out
  *.toc
  *-converted-to.*

```

# Where to store data

Where to store data, depends on the use case. Some ideas:

  * Sharing privately / among colleages:
    * Cloud storage providers 
       * [Sciebo](https://sciebo.de) (NRW universities) 
       * [pCloud](https://www.pcloud.com/eu) 
       * ...
  * For publications:
    * Domain-specific databases, e.g.
       * [SRA for sequencing data](https://www.ncbi.nlm.nih.gov/sra) 
       * ...
    * More general: 
       * [Zenodo](https://zenodo.org/)
       * [Dryad](https://datadryad.org/)
       * [Figshare](https://figshare.com/)
       * ... 
   * Assets for your project:
       * [git lfs](https://git-lfs.github.com/)

# git LFS

git LFS [(https://git-lfs.github.com/)](https://git-lfs.github.com/) is a git extension to better handle large files

* stores link to files in git repo instead whole file
* stores files itself on git LFS server on remote
* files are only downloaded when needed
* git LFS is supported by the major git hosting services, e.g. gitlab.com, github.com, bitbucket.org, ...
* Client needed, available for all major OSs, integrates as git subcommand
* On gitlab.com, git LFS needs to be enabled for the project
* Can be configured to automatically deal with files/folders/filetypes, e.g. ``git lfs track "*.psd"``
  * Stores its config in ``.gitattributes`` (don't forget to push this file, too) 


# Dos and Don'ts

 | do            | don't       | 
 | ------------- |-------------|
 | include all necessary files | pollute your repository|
 | commit often  | stuff unrelated changes in one commit|  
 | write useful commit messages |  |
 | make use of branches |  rebase a public branch|
 | merge changes back | blindly merge all changes |
 | pull before you push |forget to pull
 | |forget to push|
 | use git lfs if necessary | add large blobs directly to repo|
 |...|...|



# Workshop Summary

What we talked about:

* git is a powerful and useful tool for keeping track of your projects and collaborating with others
* basics on working with gitlab.com
* basic git CLI commands
  * ``git clone``, ``git status``, ``git pull``, ``git diff``,  ``git add``, ``git commit``, ``git push``, ``git branch``, ``git switch``, 
``git merge``, ``git rebase``, ``git checkout``, ``git help SUBCOMMAND``, ...
* how to make use of branches
* what to do and not do with git
    * merge vs rebase
    * git LFS
    * .gitignore file


---
---

# Bonus

## Links
* You can download meld here: [https://meldmerge.org/](https://meldmerge.org/)
* Github's git cheat sheet: [https://education.github.com/git-cheat-sheet-education.pdf](https://education.github.com/git-cheat-sheet-education.pdf)
* A collection of .gitignore templates: [https://github.com/github/gitignore](https://github.com/github/gitignore)

## Common operations and mishaps 

* typo in commit message
```
 git add file.py
 git commit -m "addd flie.pz"
 git commit --amend -m "add file.py"

```
* forgot to add a file
```
 git add fileA fileB
 git commit -m "add fileA, B and C"
 git add fileC
 git commit --amend -m "add fileA, B and C"
 git log

```

* I'd like to throw my local changes away - I haven't added or committed yet!
``` 
    git restore fileA
```

* See above... I added it but didn't commit!
``` 
    git restore --staged fileA # unstage; changes 
                               # are still there
    git restore fileA # discard changes for good
```

* See above... I already made a commit though...
``` 
    git log # look for previous commit hash
    git restore --source=COMMIT_HASH file_to_restore
```
 
* Why have I been working in the main branch?!
```
    git log
    git branch a_new_branch # committed changes are 
                            # now in a_new_branch

    git reset HEAD~ --hard # reset current branch 
                           # to previous commit
    # or git reset COMMIT_HASH --hard 
    git switch a_new_branch 
```
* That rebase was a bad idea...
```
   git reflog
   # look for the first 
   # entry HEAD@{XYZ} 
   # before anything that starts with 'rebase'
   git reset --hard HEAD@{XYZ}  
```

* That file was not supposed to be tracked
```
   git rm --cached thewrongfile.err
   # remove thewrongfile.err from staging area
```
* I would like to go back in time 
```
 git log # find the state you'd like to return to
 git checkout COMMIT_HASH # this will leave you 
                          # with a 'detached HEAD'
 #...
 git switch - # go back to the present
 # or branch off from here with: 
 #git switch -c new_branch

```

* I'd like to compare and modify my current fileA to its version at COMMIT (or branch other_branch)
```
 git difftool COMMIT -- fileA
 # or for fileA in other_branch
 git difftool other_branch:fileA -- fileA
```

## Customization
The git CLI is highly customizable and you can set aliases for all kinds of things.

Edit your global config: `` git config --global --edit ``


### Some alias examples

```
#...
[alias]
logadog = log --all --decorate --oneline --graph 
lg1 = log --graph --abbrev-commit --decorate --format=format:'%C(bold blue)%h%C(reset) - %C(bold green)(%ar)%C(reset)%C(white)%s%C(reset)%C(dim white)-%an%C(reset)%C(bold yellow)%d%C(reset)'--all
lg2 = log --graph --abbrev-commit --decorate --format=format:'%C(bold blue)%h%C(reset) - %C(bold cyan)%aD%C(reset) %C(bold green)(%ar)%C(reset)%C(bold yellow)%d%C(reset)%n'    '%C(white)%s%C(reset) %C(dim white)- %an%C(reset)' --all
```
Credit: [https://stackoverflow.com/questions/1057564/pretty-git-branch-graphs](https://stackoverflow.com/questions/1057564/pretty-git-branch-graphs)

#### Result git lg1
<img src="img/gitlg1.png" width="50%" height="50%">
